// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: true,
  firebase: {
    apiKey: "AIzaSyC0lGoV_fzgT16WWH7L6q8-5bNZz4ju-zI",
    authDomain: "ng-estancia.firebaseapp.com",
    databaseURL: "https://ng-estancia.firebaseio.com",
    projectId: "ng-estancia",
    storageBucket: "ng-estancia.appspot.com",
    messagingSenderId: "102394248705",
    appId: "1:102394248705:web:2a046cdac020b9fe"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
